import './App.scss';
import {useState} from "react";
import ReactDOM from "react-dom";
import * as iconImport from "@wordpress/icons";

function App() {
    const version = '9.10.0';
    const [search, setSearch] = useState('')
    const searchedIcons = Object.keys(iconImport).filter(key => key !== 'Icon' && key.toLowerCase().includes(search.toLowerCase()))
    const [active, setActive] = useState('')
    const icons = searchedIcons.map(key => {
        return {
            index: key,
            icon: iconImport[key]
        }
    })

    const versions = [
        {
            url: 'https://wp-icons.vercel.app/',
            label: '9.33.13',
        },
        {
            url: 'https://v7-wp-icons.vercel.app/',
            label: '7.0.1',
        },
        {
            url: 'https://v8-3-wp-icons.vercel.app/',
            label: '8.3.0',
        },
        {
            url: 'https://v8-2-wp-icons.vercel.app/',
            label: '8.2.0',
        },
        {
            url: 'https://v3-wp-icons.vercel.app/',
            label: '3.0.0',
        },
        {
            url: 'https://v2-wp-icons.vercel.app/',
            label: '2.0.0',
        },
        {
            url: 'https://v1-wp-icons.vercel.app/',
            label: '1.0.0',
        },
    ]

    return (
        <>
            <div className={'versions'}>
               <div className={'versions-inner '}>
                   Versionen:
                   <ul>
                       {versions.map((v, key) => (
                           <li key={key}>
                               <a className={version === v.label ? 'active' : ''} href={v.url}>{v.label}</a>
                           </li>
                       ))}
                   </ul>
               </div>
            </div>
            <main>
                <header>
                    <h1>@wordpress/icons:{version}</h1>
                    <input autoFocus={true} placeholder={'Suchen...'} type={'search'} value={search} onChange={e => setSearch(e.target.value)} />
                    <span className={'count-results'}>{icons.length} Icons gefunden</span>
                </header>
                <section>
                    {icons.map((x, key) => <IconItem key={key} onClick={setActive} {...x} />)}
                </section>
                <IconModal icon={iconImport[active]} index={active} onClose={() => setActive('')} open={active !== ''} />
            </main>
        </>
    );
}

const IconItem = ({icon, index, onClick = () => {}}) => {
    return (
        <>
            <button className={'icon-item'} onClick={() => onClick(index)} >
                <iconImport.Icon icon={icon} />
                <span>{index}</span>
            </button>
        </>
    )
}

const IconModal = ({icon, index, onClose, open}) => {
    return ReactDOM.createPortal((
        <div className={'icon-modal' + (open ? ' open' : '')}>
            <button className={'overlay'} onClick={onClose} />
            <div className={'inner'}>
                <button className={'close'} onClick={onClose}>
                    <iconImport.Icon icon={iconImport.close} />
                </button>
                <div className={'icon'}>
                    {icon && <iconImport.Icon icon={icon} />}
                    <span className={'title'}>{index}</span>
                </div>
                <div className={'codes'}>
                    <span className={'title'}>Copy code snippet</span>
                    <code>{index}</code>
                    <code>{`import {Icon, ${index}} from "@wordpress/icons";`}</code>
                    <code>{`<Icon icon={${index}} />`}</code>
                </div>
            </div>
        </div>
    ), document.body)
}

export default App;
